package com.segmentify.app.config;


import com.segmentify.app.domain.Product;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.communication.tcp.TcpCommunicationSpi;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.apache.ignite.springdata22.repository.config.EnableIgniteRepositories;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableIgniteRepositories(value = "com.segmentify.app.*")
public class IgniteCacheConfiguration {

    @Bean
    public Ignite igniteInstance() {
        return Ignition.start(igniteConfiguration());
    }

    @Bean
    public IgniteCache<String, Product> igniteCache() {
        IgniteCache<String, Product> testIgniteInstance = igniteInstance().cache("Product");
        return testIgniteInstance;
    }

    @Bean(name = "igniteConfiguration")
    public IgniteConfiguration igniteConfiguration() {
        IgniteConfiguration igniteConfiguration = new IgniteConfiguration();
        igniteConfiguration.setIgniteInstanceName("testIgniteInstance");
        //igniteConfiguration.setClientMode(true);
        igniteConfiguration.setPeerClassLoadingEnabled(true);
        igniteConfiguration.setLocalHost("0.0.0.0");

        TcpDiscoverySpi tcpDiscoverySpi = new TcpDiscoverySpi();
        TcpDiscoveryMulticastIpFinder ipFinder = new TcpDiscoveryMulticastIpFinder();
        ipFinder.setAddresses(Collections.singletonList("0.0.0.0:47500..47509"));
        tcpDiscoverySpi.setIpFinder(ipFinder);
        tcpDiscoverySpi.setLocalPort(47500);
        // Changing local port range. This is an optional action.
        tcpDiscoverySpi.setLocalPortRange(9);
        tcpDiscoverySpi.setLocalAddress("0.0.0.0");
        igniteConfiguration.setDiscoverySpi(tcpDiscoverySpi);

        TcpCommunicationSpi communicationSpi = new TcpCommunicationSpi();
        communicationSpi.setLocalAddress("0.0.0.0");
        communicationSpi.setLocalPort(48100);
        communicationSpi.setSlowClientQueueLimit(1000);
        igniteConfiguration.setCommunicationSpi(communicationSpi);

        igniteConfiguration.setCacheConfiguration(cacheConfiguration());

        DataStorageConfiguration storageConfiguration = new DataStorageConfiguration();
        storageConfiguration.getDefaultDataRegionConfiguration().setPersistenceEnabled(true);
        storageConfiguration.setStoragePath("C:\\Users\\hacko\\Desktop\\deneme\\spring-boot-ignite\\storagedb");
        igniteConfiguration.setDataStorageConfiguration(storageConfiguration);

        return igniteConfiguration;

    }

    @Bean(name = "cacheConfiguration")
    public CacheConfiguration[] cacheConfiguration() {
        List<CacheConfiguration> cacheConfigurations = new ArrayList<>();

        CacheConfiguration orgCacheCfgProduct = new CacheConfiguration<>("Product");
        orgCacheCfgProduct.setCacheMode(CacheMode.REPLICATED); // Default.
        orgCacheCfgProduct.setIndexedTypes(String.class, Product.class);
        cacheConfigurations.add(orgCacheCfgProduct);

        return cacheConfigurations.toArray(new CacheConfiguration[cacheConfigurations.size()]);
    }
}
