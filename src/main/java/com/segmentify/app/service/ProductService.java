package com.segmentify.app.service;


import com.segmentify.app.domain.Product;
import com.segmentify.app.repository.ProductRepository;
import org.apache.ignite.IgniteCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.cache.processor.MutableEntry;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.Random;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    IgniteCache<String, Product> igniteCache;

    public void createProduct() {
        String id = String.valueOf(new Random().nextLong());
        productRepository.save(id, new Product());
    }

    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    public void create_20000k_product() {
        for (long i = 1; i <= 20000; i++) {
            String id = String.valueOf(i);
            productRepository.save(id, buildProduct(id));
        }
    }

    public long count() {
        return productRepository.count();
    }



    public String deleteById(String id) {
        String message;

        if (productRepository.existsById(id)) {
            productRepository.deleteById(id);
            return message = "Ürününüz başarılı bir şekilde silindi";
        }

        return message = "Silmke istediginiz ürününüz db'de bulunamamıştır.";
    }


    public Product getRandomProduct() {
        int number = new Random().nextInt(20000) + 1;

        String id = String.valueOf(number);

        return productRepository.findById(id).get();

    }


    private void updateProduct(MutableEntry<String, Product> mutableEntry, String name, Integer id) {
        Product product = mutableEntry.getValue();
        product.setName(name);
        mutableEntry.setValue(product);
    }

    private Product buildProduct(String id) {
        Product product = new Product();

        product.setProductId(id);
        product.setName("bilgisayar");
        product.setUrl("www.dell.com.tr");
        product.setImage("www.google.com/dell_bilgisayar");
        product.setPrice(345.23);
        product.setOldPrice(453.32);
        product.setCategoryList(getCategoryList());
        product.setBrand("dell");
        product.setInsertTime(new Timestamp(2323));
        product.setLastUpdateTime(new Timestamp(2311));
        product.setInStock(true);
        product.setGender("dell");
        product.setSizes(getSizeArrayList());
        product.setColors(getColorsArrayList());
        product.setParams(getParamsMap());

        return product;
    }

    private ArrayList<String> getSizeArrayList() {
        ArrayList<String> sizes = new ArrayList<String>();

        sizes.add("small");
        sizes.add("medium");
        sizes.add("large");

        return sizes;
    }

    private ArrayList<String> getColorsArrayList() {
        ArrayList<String> sizes = new ArrayList<String>();

        sizes.add("black");
        sizes.add("white");
        sizes.add("purple");

        return sizes;
    }

    private HashMap<String, String> getParamsMap() {
        HashMap<String, String> params = new HashMap<>();

        params.put("bugra", "iyi satıcı");
        params.put("tugberk", "iyi satıcı");

        return params;
    }

    private ArrayList<String> getCategoryList() {
        ArrayList<String> categoryList = new ArrayList<>();

        categoryList.add("teknoloji kategorisi");
        categoryList.add("indirim kategorisi");
        categoryList.add("pahalı eşya kategorisi");

        return categoryList;
    }

    public Product getById(Integer id) {
        String s_id = String.valueOf(id);
        Optional<Product> optionalProduct = productRepository.findById(s_id);
        return optionalProduct.get();
    }

    public boolean existById(Integer id) {
        return productRepository.existsById(String.valueOf(id));
    }
}
