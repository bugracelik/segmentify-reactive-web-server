package com.segmentify.app.domain;

import lombok.Data;
import org.apache.ignite.cache.affinity.AffinityKeyMapped;
import org.apache.ignite.cache.query.annotations.QuerySqlField;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Data
public class Product {
    @AffinityKeyMapped
    @QuerySqlField(index = true, inlineSize = 20)
    private String productId; // ürün identifier
    @QuerySqlField(index = true)
    private String name; // ürün ismi
    private String url; // ürün linki
    private String image; // ürün resmi
    private Double price; // ürün fiyatı
    private Double oldPrice; // ürün eski fiyatı
    private List<String> categoryList; // ürün kategorileri
    private String brand; // ürün markası
    private Timestamp insertTime; // ürün sisteme giriş zamanı
    private Timestamp lastUpdateTime; // ürün son güncelleme zamanı
    private boolean inStock; // ürün stok bilgisi
    private String gender; // ürün cinsiyeti
    private List<String> sizes; // ürün bedenleri
    private List<String> colors; // ürün renkleri
    private Map<String, String> params; // ürüne dair custom diğer bilgiler

    public Product() {
    }

    public Product(String productId, String name) {
        this.productId = productId;
        this.name = name;
    }

    public Product(String productId, String name, String url, String image, Double price, Double oldPrice, List<String> categoryList, String brand, Timestamp insertTime, Timestamp lastUpdateTime, boolean inStock, String gender, List<String> sizes, List<String> colors, Map<String, String> params) {
        this.productId = productId;
        this.name = name;
        this.url = url;
        this.image = image;
        this.price = price;
        this.oldPrice = oldPrice;
        this.categoryList = categoryList;
        this.brand = brand;
        this.insertTime = insertTime;
        this.lastUpdateTime = lastUpdateTime;
        this.inStock = inStock;
        this.gender = gender;
        this.sizes = sizes;
        this.colors = colors;
        this.params = params;
    }
}
