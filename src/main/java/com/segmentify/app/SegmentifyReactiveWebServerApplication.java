package com.segmentify.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SegmentifyReactiveWebServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SegmentifyReactiveWebServerApplication.class, args);
	}

}
