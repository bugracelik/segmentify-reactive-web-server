package com.segmentify.app.repository;


import com.segmentify.app.domain.Product;
import org.apache.ignite.springdata22.repository.IgniteRepository;
import org.apache.ignite.springdata22.repository.config.RepositoryConfig;

@RepositoryConfig(cacheName = "Product")
public interface ProductRepository extends IgniteRepository<Product, String> {
}
