package com.segmentify.app.controller;

import com.segmentify.app.domain.Product;
import com.segmentify.app.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    @Autowired
    ProductService productService;


    @PostMapping("/create")
    public void createProduct() {
        productService.createProduct();
    }

    @GetMapping("/all")
    public Iterable<Product> getAllProduct() {
        return productService.findAll();
    }

    @PostMapping("/create_20000_product")
    public void createProduct_20000k() {
        productService.create_20000k_product();
    }

    @GetMapping("/count")
    public long countProduct() {
        return productService.count();
    }

    @GetMapping("/id/{id}")
    public Product getById(@PathVariable Integer id){
        return productService.getById(id);
    }

    @GetMapping("/random")
    public Product getRandomProduct() {
        return productService.getRandomProduct();
    }

    @GetMapping("/exist/{id}")
    public boolean bar(@PathVariable Integer id){
        return productService.existById(id);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteById(@PathVariable String id) {
        return productService.deleteById(id);
    }

}
